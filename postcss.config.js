module.exports = {
  parser: 'sugarss',
  plugins: {
    'postcss-import': {},
    'postcss-nested': {},
    'postcss-cssnext': {},
    'postcss-css-variables': {},
  },
  env: {
    production: {
      cssnano: { autoprefixer: false },
    },
  },
}
