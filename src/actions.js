// @flow
import { provideState } from 'freactal'
import { get } from 'superagent'
import { memoize } from 'lodash/fp'

const searchCommits = memoize(q =>
  get('https://api.github.com/search/commits')
    .query({ q, sort: 'committer-date' })
    .set({ Accept: 'application/vnd.github.cloak-preview' })
    .then(x => x.body.items))

const wrapWithPending = (pendingKey: string, cb: Function) =>
  (effects, ...a) =>
    effects
      .setFlag(pendingKey, true)
      .then(() => cb(effects, ...a))
      .then(value => effects.setFlag(pendingKey, false).then(() => value))

const Provider = provideState({
  initialState: () => ({
    commits: [],
    searchPending: false,
  }),
  effects: {
    setFlag: (effects, key: string, value: boolean) => state => ({ ...state, [key]: value }),
    clearCommits: () => state => ({ ...state, commits: [] }),
    searchCommits: wrapWithPending('searchPending', (effects, str) =>
      searchCommits(str)
        .then(commits => state => ({ ...state, commits })),
    ),
  },
})

export default Provider
