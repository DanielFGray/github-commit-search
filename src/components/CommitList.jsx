// @flow
import React from 'react'
import ago from 's-ago'
import style from '../style.sss'

const CommitList = ({ list }: { list: Array<Object> }) => (
  <div className={style.commitList}>
    {list.map(e => <Commit key={e.sha} {...e} />)}
  </div>
)

const Commit = ({
  html_url,
  commit,
  repository,
}: {
  html_url: string,
  commit: Object,
  repository: Object,
}) => {
  const date = new Date(commit.committer.date)
  return (
    <div className={style.commit}>
      <span className={style.message}>
        <a href={html_url} title={commit.message} target="_blank" rel="noopener noreferrer">
          {commit.message.split('\n')[0]}
        </a>
      </span>
      <span className={style.repo}>
        <a href={repository.html_url} target="_blank" rel="noopener noreferrer">
          {repository.full_name}
        </a>
      </span>
      <span className={style.date}>
        <a title={date.toLocaleString()}>
          {ago(date)}
        </a>
      </span>
    </div>
  )
}

export default CommitList
