// @flow
import React, { Component } from 'react'
import { injectState } from 'freactal'
import Spinner from '../components/Spinner'
import CommitList from '../components/CommitList'
import style from '../style.sss'

class Home extends Component {
  props: {
    effects: {
      searchCommits: Function,
      clearCommits: Function,
    },
    state: {
      commits: Array<Object>,
      searchPending: boolean,
    },
  }

  state = {
    searchText: 'my hands are typing words', // https://xkcd.com/1296/
  }

  componentWillMount() {
    this.getCommits()
  }

  getCommits = (e?: SyntheticEvent) => {
    if (e) e.preventDefault()
    this.props.effects.clearCommits()
    this.props.effects.searchCommits(this.state.searchText)
  }

  searchChange = (e: SyntheticInputEvent) => {
    this.setState({ searchText: e.target.value })
  }

  render() {
    return (
      <div>
        <header>
          <div className={style.search}>
            <form onSubmit={this.getCommits}>
              <input
                type="text"
                value={this.state.searchText}
                onChange={this.searchChange}
              />
            </form>
          </div>
        </header>
        {this.props.state.searchPending && <Spinner />}
        <CommitList list={this.props.state.commits} />
        <footer>
          <div>
            <a href="https://gitlab.com/danielfgray/github-commit-search" target="_blank" rel="noopener noreferrer">
              source
            </a>
          </div>
        </footer>
      </div>
    )
  }
}

export default injectState(Home)
